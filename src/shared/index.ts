export {
  CONFIG_NAME, PACKAGE_FILE, DEAD_ENDS,
  YARNRC, YARN_LOCK, TS_CONFIG, LOCATION, CONFIG_FILE,
} from './constants';

export { Project, ProjectTemplate, ProjectsFile, Dependency, MultiDependency, MonoDependency } from './types';

export { JSONSchemaForNPMPackageJsonFiles as PackageJSON } from '@schemastore/package';
