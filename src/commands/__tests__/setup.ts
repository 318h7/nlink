import mockFS from 'mock-fs';

import { getConfig, getPackageFile, Mock, getMockArgs } from '../../__test__/utils';
import setup from '../setup';
import { CONFIG_FILE, PACKAGE_FILE, CONFIG_NAME } from '../../shared';
import { isFile } from '../../util';
import { link } from '../../util/system';
import * as files from '../../util/files';

jest.mock('../../util/system');
jest.spyOn(files, 'writeFile');

describe('commands/setup', () => {
  afterEach(() => {
    mockFS.restore();
    const mockLink = link as Mock<typeof link>;
    mockLink.mockClear();
    const mockWrite = files.writeFile as Mock<typeof files.writeFile>;
    mockWrite.mockClear();
  });

  it('creates config file', async () => {
    mockFS({ '/test': { [PACKAGE_FILE]: getPackageFile({ name: 'a' }) } });

    await setup(getMockArgs({ force: false }));
    const isConfigPresent = isFile(CONFIG_FILE);

    expect(files.writeFile).toHaveBeenCalled();
    expect(isConfigPresent).toBe(true);
  });

  it('does not create config if no projects were found', async () => {
    mockFS({ '/test': {} });

    await setup(getMockArgs({ force: false }));
    const isConfigPresent = isFile(CONFIG_FILE);

    expect(files.writeFile).not.toHaveBeenCalled();
    expect(isConfigPresent).toBe(false);
  });

  it('links found projects', async () => {
    mockFS({
      '/test/deeper': { [PACKAGE_FILE]: getPackageFile({ name: 'a' }) },
      '/test/another_one': { [PACKAGE_FILE]: getPackageFile({ name: 'b' }) },
      '/test/and_another_one': { [PACKAGE_FILE]: getPackageFile({ name: 'c' }), 'yarn.lock': 'dummy' },
    });

    await setup(getMockArgs({ force: false }))

    expect(link).toHaveBeenNthCalledWith(1, '/test/and_another_one', true);
    expect(link).toHaveBeenNthCalledWith(2, '/test/another_one', false);
    expect(link).toHaveBeenNthCalledWith(3, '/test/deeper', false);
    expect(link).toHaveBeenCalledTimes(3);
  });

  it('reads existing config if present and relinks projects', async () => {
    mockFS({
      '/test': {
        [CONFIG_NAME]: getConfig({ name: 'a' })
      }
    });

    await setup(getMockArgs({ force: false }))

    expect(link).toHaveBeenCalledWith('/test/a', false);
    expect(files.writeFile).not.toHaveBeenCalled();
  });

  it('rewrites existing config if force option is passed', async () => {
    mockFS({
      '/test': {
        [CONFIG_NAME]: getConfig({ name: 'a' }),
        'deeper': { [PACKAGE_FILE]: getPackageFile({ name: 'a' }) },
      }
    });

    await setup(getMockArgs({ force: true }));

    expect(files.writeFile).toHaveBeenCalled();
    expect(link).toHaveBeenCalledWith('/test/deeper', false);
  });
});
