# [1.3.0](https://gitlab.com/318h7/nlink/compare/v1.2.0...v1.3.0) (2020-09-17)


### Bug Fixes

* link issue, add logging, add async loop ([8bc3476](https://gitlab.com/318h7/nlink/commit/8bc34761e62e88613606748f781af6f1cf2bf550))


### Features

* introduce async, update tests ([566d0d6](https://gitlab.com/318h7/nlink/commit/566d0d6d0bf8c93834af9f6ae12628ce262d6ccf))

# [1.2.0](https://gitlab.com/318h7/nlink/compare/v1.1.1...v1.2.0) (2020-09-16)


### Bug Fixes

* type error ([8399034](https://gitlab.com/318h7/nlink/commit/8399034addfb663f3d6154524f0a3774c6e77acb))


### Features

* implement unlinking ([b4264d7](https://gitlab.com/318h7/nlink/commit/b4264d708741eeac02060660eae354443b76c862))

## [1.1.1](https://gitlab.com/318h7/nlink/compare/v1.1.0...v1.1.1) (2020-09-16)


### Bug Fixes

* allow higher complexity ([489af5f](https://gitlab.com/318h7/nlink/commit/489af5f2df77e72318801f09e05ab2c08ce223bf))
* link issues, finish link tests ([538020e](https://gitlab.com/318h7/nlink/commit/538020eb7337fa36268439e60d36d5a8bb346594))

# [1.1.0](https://gitlab.com/318h7/nlink/compare/v1.0.5...v1.1.0) (2020-09-13)


### Bug Fixes

* local test command ([df793a4](https://gitlab.com/318h7/nlink/commit/df793a4b0b342d0b21fe789be22c21264c57b0c9))


### Features

* add inside-out linking, list linking, tests ([fcf3e84](https://gitlab.com/318h7/nlink/commit/fcf3e840babef6d5068eaec0175cb4c2b0c090a4))

## [1.0.5](https://gitlab.com/318h7/nlink/compare/v1.0.4...v1.0.5) (2020-09-13)


### Bug Fixes

* add build step to pipeline ([fe07f56](https://gitlab.com/318h7/nlink/commit/fe07f561e16d6900620d48edcf691a926515a4c5))

## [1.0.4](https://gitlab.com/318h7/nlink/compare/v1.0.3...v1.0.4) (2020-09-13)


### Bug Fixes

* remove assets from release ([9c488b2](https://gitlab.com/318h7/nlink/commit/9c488b2c4d0b2fe361231425314cf826a6e8cf9e))

## [1.0.3](https://gitlab.com/318h7/nlink/compare/v1.0.2...v1.0.3) (2020-09-12)


### Bug Fixes

* change npm access to public ([c1bbc2f](https://gitlab.com/318h7/nlink/commit/c1bbc2f5b041ba43bf715a29aa53b2e9d5384f51))

## [1.0.2](https://gitlab.com/318h7/nlink/compare/v1.0.1...v1.0.2) (2020-09-12)


### Bug Fixes

* release archive ([47c898e](https://gitlab.com/318h7/nlink/commit/47c898eb3ba457df542c81bbbe74a027878a102c))

## [1.0.1](https://gitlab.com/318h7/nlink/compare/v1.0.0...v1.0.1) (2020-09-12)


### Performance Improvements

* improve gitlab caching ([f2be5fb](https://gitlab.com/318h7/nlink/commit/f2be5fb3cae8e824a88c738b695d7dcbc145543c))

# 1.0.0 (2020-09-12)


### Bug Fixes

* ci file ([3723ec0](https://gitlab.com/318h7/nlink/commit/3723ec043b59b2f221b2a44f4436f74d08bc9162))
* config, lint errors, typecheck tests ([151fa01](https://gitlab.com/318h7/nlink/commit/151fa01a81940ea9ba0a88f4337bf38d2d2bd9f0))
* gitlab node image ([93cd5ef](https://gitlab.com/318h7/nlink/commit/93cd5ef3cb6ccc0394dd50a4e40957b34f18726c))
* lint, ts issues ([d788515](https://gitlab.com/318h7/nlink/commit/d78851550e7620f1e44d295bf006831374d7a6ac))


### Features

* add semantic release ([940d6b2](https://gitlab.com/318h7/nlink/commit/940d6b21371de1178f8daceeecf75589fae4dd42))
* implement unnamed link from root and tests ([4dfe26d](https://gitlab.com/318h7/nlink/commit/4dfe26d7337356c02b7fd7a53fe1598724365367))
