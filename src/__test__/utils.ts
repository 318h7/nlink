import { Arguments } from 'yargs';

import { Project, LOCATION, CONFIG_NAME, ProjectsFile, Dependency } from '../shared';
import * as consts from '../shared/constants';

interface ProjectTemplate {
  name: string,
  dependencies?: string[],
  depending?: string[],
  nodeName?: string,
}

type SetMockLocation = (newLocation: string) => void;
export const setMockLocation: SetMockLocation = newLocation => {
  /* eslint-disable import/namespace, @typescript-eslint/ban-ts-comment */
  // @ts-ignore
  consts.LOCATION = newLocation;
  // @ts-ignore
  consts.CONFIG_FILE = `${newLocation}/${CONFIG_NAME}`;
  /* eslint-disable */
};

type GetPackageFile = (template: ProjectTemplate) => string;
export const getPackageFile: GetPackageFile = ({ name, dependencies = [] }) => JSON.stringify({
  name,
  dependencies: dependencies.reduce((deps, name) => ({ ...deps, [name]: '1.0.0' }), {}),
});

type GetProject = (template: ProjectTemplate) => Project;
export const getProject: GetProject = ({ name, dependencies = [], depending = [], ...rest }) => ({
  name,
  nodeName: name,
  path: `${LOCATION}/${name}`,
  ts: false,
  yarn: false,
  dependencies,
  depending,
  ...rest
});

type GetConfig = (...args: ProjectTemplate[]) => string;
export const getConfig: GetConfig = (...args) => JSON.stringify(
  args.reduce<ProjectsFile>(
    (projects, template, _, array) => {
      const depending = array.reduce<string[]>((result, { name: dependingName, dependencies }) => {
        if (dependencies?.some(name => name === template.nodeName || name === template.name)) {
          result.push(dependingName);
        }
        return result;
      }, []);
      projects.projects.push(getProject({ depending, ...template }));
      return projects;
    },
    { projects: [] }
  )
);

interface MockWorld {
  [name: string]: string;
}

type CreateMockWorld = (...args: ProjectTemplate[]) => MockWorld;
export const createMockWorld: CreateMockWorld = (...args) => {
  const world: MockWorld = {
    [CONFIG_NAME]: getConfig(...args),
  }

  args.forEach(template => {
    world[template.name] = getPackageFile(template);
  });

  return world;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AnyFunction = (...args: any[]) => any;
export type Mock<T extends AnyFunction> = jest.MockedFunction<T>;

type AnyObject = Record<string, unknown>;
type GetMockArgs = (args?: AnyObject) => Arguments<AnyObject>;
export const getMockArgs: GetMockArgs = (args = {}) => ({
  _: [],
  '$0': 'test',
  ...args,
})

type DummyExit = () => never;
const dummy = (): void => {
  // do nothing
  return;
};

export const dummyExit = dummy as DummyExit;

export const mockDep = (path: string, yarn: boolean, dependencies: string | string[]): Dependency => {
  const template = { path, yarn };
  if (Array.isArray(dependencies)) {
    return { names: dependencies as string[], ...template }
  } else {
    return { nodeName: dependencies as string, ...template }
  }
}
