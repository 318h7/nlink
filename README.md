# nlink

![pipeline](https://gitlab.com/318h7/nlink/badges/master/pipeline.svg) ![coverage](https://gitlab.com/318h7/nlink/badges/master/coverage.svg) [![npm version](https://badge.fury.io/js/%40318h7%2Fnlink.svg)](https://www.npmjs.com/package/@318h7/nlink) [![TypeScript](https://badgen.net/badge/icon/typescript?icon=typescript&label)](https://www.typescriptlang.org/) [![MIT Licence](https://badgen.net/npm/license/lodash)](https://gitlab.com/318h7/nlink/-/blob/master/LICENSE.md) [![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest) [![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](https://gitlab.com/318h7/nlink/-/blob/master/CONTRIBUTING.md) 

**nlink** is a CLI utility to aid linking Node.js projects for development

* wrapper around `npm\yarn link`
* analyzes multi-repo folder to create a map of interdependent projects
* links projects to bring mono-repo powers to multi-repos
* TypeScript friendly
* contextual actions to aid linking during development

## Status

The project is in beta phase. PRs are very welcome. As well as issues and [suggestions](https://gitlab.com/318h7/nlink/-/requirements_management/requirements) ❤

If something isn't working for you, do raise an [issue](https://gitlab.com/318h7/nlink/-/issues). 

**Work in progress**:

* Typescript type safe linking
* Scope (wildcard) matching

**Planned functionality**:

* Watch mode (link and wait, then unlink on quit)

## Installation

### yarn
```shell
yarn global add @318h7/nlink
```

### npm
```shell
npm install -g @318h7/nlink
```

**Note:**
You will probably need to run the commands with `sudo`

## Usage

```shell
nlk [command]

Commands:
  nlk setup          Creates project map in the current location
  nlk link [name]    Links project dependencies
  nlk unlink [name]  Uninks project dependencies

Options:
  --help     Show help                                                 [boolean]
  --version  Show version number                                       [boolean]
```

## Description

### Setup
To analyze a folder and link all developed projects together:

```shell
cd projects/my_multy_repo
nlk setup
nlk link
```

The setup command will traverse current directory in search of node projects,
and save a map in the file .nlink.json
Don't be afraid to edit the file manually according to your needs, it's just a plane JSON.
After creating the file it will run `yarn/npm link` to register projects for linking.
Running `link` command without parameters will link together all developed projects.
Consequent runs of `setup` will just prepare the links.

In order to regenerate the projects map:

``` shell
nlk setup --force
```

### Linking 🔗

#### Info

* **nlink** accepts the package name (**name** field in the *package.json*) or the folder name where the project is located as a dependency name.

* **nlink** will detect if project is using `npm` or `yarn` as dependency manager and execute the appropriate command.

* **nlink** will also detect if project is using TypeScript and check if any other project dependencies depend on the same package
you are trying to link. It will link all related projects, to avoid type errors in case of incompatible type update in related projects.

#### Commands
To setup links for all project dependencies: 

```shell
cd project_in_question
nlk link
```

Note: If run from a projects that has no dependencies, creates links in all projects that depend on that package:


To link a dependency:

```shell
cd depending_project
nlk link <dependency>
```
Note: If run from a project that has no named dependency, will try to link current directory project in the named one.

To link couple of packages:

```shell
cd depending_project
nlk link <dependency_A> <dependency_B>
```

To link scoped packages:

```shell
cd depending_project
nlk link @example/**
```

Will link all dependencies that have @example scope in the name.

🦄 To link all the dependencies in the multi-repo:

```shell
cd <folder with .nlink.json>
nlk link
```

