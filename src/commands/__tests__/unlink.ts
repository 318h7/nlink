import mockFS from 'mock-fs';

import { getMockArgs, dummyExit, createMockWorld, Mock, setMockLocation } from '../../__test__/utils';
import ul from '../unlink';
import { unlink } from '../../util/system';

jest.mock('../../util/system');

const mockExit = jest.spyOn(process, 'exit').mockImplementation(dummyExit);

describe('commands/unlink', () => {
  beforeEach(() => {
    setMockLocation('/test/dev');
  });

  afterAll(() => {
    jest.restoreAllMocks();
  });

  afterEach(() => {
    mockFS.restore();
    const mockUnlink = unlink as Mock<typeof unlink>;
    mockUnlink.mockClear();
    mockExit.mockClear();
  })

  it('quits if no .nlink json was found', async () => {
    mockFS({ '/test/dev': {} });

    await ul(getMockArgs({ name: 'who cares?' }));
    await unlink;

    expect(unlink).not.toHaveBeenCalled();
    expect(mockExit).toHaveBeenCalled();
  });

  it('does nothing if no dependencies found', async () => {
    mockFS({
      '/test/dev': createMockWorld({ name: 'test' }, { name: 'foo' }, { name: 'bar' })
    });
    setMockLocation('/test/dev');

    await ul(getMockArgs({ name: 'foo' }));
    await unlink;

    expect(unlink).not.toHaveBeenCalled();
  });

  describe('from root folder', () => {
    it('calls link on all projects that have name in dependenices', async () => {
      mockFS({
        '/test/dev': createMockWorld(
          { name: 'test', dependencies: ['foo'] },
          { name: 'bar', dependencies: ['foo'] },
          { name: 'foo' },
        )
      });
      setMockLocation('/test/dev');

      await ul(getMockArgs({ name: 'foo' }));
      await unlink;

      expect(unlink).toHaveBeenNthCalledWith(1, '/test/dev/test', false, ['foo']);
      expect(unlink).toHaveBeenNthCalledWith(2, '/test/dev/bar', false, ['foo']);
    });

    it('calls link only on related projects', async () => {
      mockFS({
        '/test/dev': createMockWorld(
          { name: 'test', dependencies: ['foo'] },
          { name: 'bar' },
          { name: 'foo', dependencies: ['bar'] },
        )
      });
      setMockLocation('/test/dev');

      await ul(getMockArgs({ name: 'foo' }));
      await unlink;

      expect(unlink).toHaveBeenNthCalledWith(1, '/test/dev/test', false, ['foo']);
      expect(unlink).toHaveBeenCalledTimes(1);
    });

    it('does nothing if name not found among dependencies', async () => {
      mockFS({
        '/test/dev': createMockWorld(
          { name: 'test', dependencies: ['foo'] },
          { name: 'bar', dependencies: [ 'random', 'things', 'test' ] },
          { name: 'foo', dependencies: ['bar'] },
        )
      });
      setMockLocation('/test/dev');

      await ul(getMockArgs({ name: 'elephant' }));
      await unlink;

      expect(unlink).not.toHaveBeenCalled();
    });

    it('passing no name links everything in developed projects', async () => {
      mockFS({
        '/test/dev': createMockWorld(
          { name: 'test', dependencies: ['foo', 'bar'] },
          { name: 'bar', dependencies: [ 'random', 'things', 'test' ] },
          { name: 'foo', dependencies: ['bar'] },
          { name: 'shmeck', dependencies: ['nothing', 'related', 'here'] },
        )
      });
      setMockLocation('/test/dev');

      await ul(getMockArgs());
      await unlink;

      expect(unlink).toHaveBeenNthCalledWith(1, '/test/dev/test', false, ['foo', 'bar']);
      expect(unlink).toHaveBeenNthCalledWith(2, '/test/dev/bar', false, ['test']);
      expect(unlink).toHaveBeenNthCalledWith(3, '/test/dev/foo', false, ['bar']);
      expect(unlink).toHaveBeenCalledTimes(3);
    });
  });

  describe('from project folder', () => {
    describe('named', () => {
      it('links the project if called from depender', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['foo', 'bar'] },
            { name: 'bar', dependencies: [ 'foo' ] },
            { name: 'foo', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ul(getMockArgs({ name: 'foo' }));
        await unlink;

        expect(unlink).toHaveBeenCalledWith('/test/dev/bar', false, ['foo']);
      });

      it('links the project when called from dependency', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['foo', 'bar'] },
            { name: 'bar', dependencies: [ 'foo' ] },
            { name: 'foo', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ul(getMockArgs({ name: 'test' }));
        await unlink;

        expect(unlink).toHaveBeenCalledWith('/test/dev/test', false, ['bar']);
      });

      it('links couple of projects when called from dependency', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['foo'] },
            { name: 'bar', dependencies: [ 'foo', 'test', 'spam' ] },
            { name: 'foo', dependencies: [] },
            { name: 'spam', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ul(getMockArgs({ name: 'foo', _: ['link', 'test', 'spam'] }));
        await unlink;

        expect(unlink).toHaveBeenCalledWith('/test/dev/bar', false, ['foo', 'test', 'spam']);
      });

      it('does nothing when project has no such dependency', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['bar'] },
            { name: 'bar', dependencies: [ 'foo' ] },
            { name: 'foo', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/test');

        await ul(getMockArgs({ name: 'foo' }));
        await unlink;

        expect(unlink).not.toHaveBeenCalled();
        expect(mockExit).toHaveBeenCalled();
      });
    });

    describe('unnamed call', () => {
      it('quits if there are no dependencies and no project depend on it', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: [] },
            { name: 'bar', dependencies: [] },
            { name: 'foo', dependencies: ['spam'] },
            { name: 'spam', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ul(getMockArgs());
        await unlink;

        expect(mockExit).toHaveBeenCalled();
      });

      it('links all current project dependencies', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: [] },
            { name: 'bar', dependencies: ['test'] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ul(getMockArgs());
        await unlink;

        expect(unlink).toHaveBeenCalledWith('/test/dev/bar', false, ['test']);
      });

      it('links to itself in all projects if has no dependencies', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['bar'] },
            { name: 'bar', dependencies: [] },
            { name: 'foo', dependencies: ['spam'] },
            { name: 'spam', dependencies: ['bar'] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ul(getMockArgs());
        await unlink;

        expect(unlink).toHaveBeenNthCalledWith(1, '/test/dev/test', false, ['bar']);
        expect(unlink).toHaveBeenNthCalledWith(2, '/test/dev/spam', false, ['bar']);
        expect(unlink).toHaveBeenCalledTimes(2);
      });

      it('prefers current project if has both depending and depends on others', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['bar'] },
            { name: 'bar', dependencies: ['test'] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ul(getMockArgs());
        await unlink;

        expect(unlink).toHaveBeenCalledWith('/test/dev/bar', false, ['test']);
      });

      it('links up multiple projects ', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            // TODO: should this really be allowed? Throw or deal safely?
            { name: 'bar', dependencies: ['test', 'foo', 'spam', 'trash'] },
            { name: 'test', dependencies: [] },
            { name: 'foo', dependencies: [] },
            { name: 'spam', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ul(getMockArgs());
        await unlink;

        expect(unlink).toHaveBeenCalledWith('/test/dev/bar', false, ['test', 'foo', 'spam']);
      });
    });
  });
});
