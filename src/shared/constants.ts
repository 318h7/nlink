export const LOCATION: string = process.env.INIT_CWD || process.env.PWD || '';

export const CONFIG_NAME = '.nlink.json';
export const PACKAGE_FILE = 'package.json';
export const YARNRC = '.yarnrc';
export const YARN_LOCK = 'yarn.lock';
export const TS_CONFIG = 'tsconfig.json';
export const CONFIG_FILE = `${LOCATION}/${CONFIG_NAME}`;

export const DEAD_ENDS = [
  'node_modules',
  'bin',
  'dist',
  'build',
  '.git',
  'public',
  'resources',
  'src',
  'coverage',
];
