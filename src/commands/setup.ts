import { Arguments } from 'yargs';
import { green } from 'chalk';

import { Project, LOCATION, CONFIG_FILE, ProjectTemplate } from '../shared';
import {
  traverse, analyseDependencies, createProject, isNodeProject, isYarnProject, isTSProject,
  getFolder, readPackage, readConfig, writeFile, link, isFile, asyncForEach
} from '../util';

interface SetupParams {
  force?: boolean,
  f?: boolean,
}

type GetProject = (files: string[], path: string) => ProjectTemplate;
const getProject: GetProject = (files, path) => createProject(
  readPackage(path),
  isYarnProject(files),
  isTSProject(files),
  path,
  getFolder(path),
);

type CollectProjects = (files: string[], path: string) => ProjectTemplate | null;
const collectProjects: CollectProjects = (files, path) => {
  if (isNodeProject(files)) {
    return getProject(files, path);
  }
  return null;
}

type Setup = (params: Arguments<SetupParams>) => Promise<void>;
const setup: Setup = async ({ force = false }) => {
  const isConfigPresent = isFile(CONFIG_FILE);
  let projects: Project[] = [];

  if (!isConfigPresent || force) {
    projects = analyseDependencies(
      traverse<ProjectTemplate>(LOCATION, collectProjects),
    );
    if (projects.length) {
      writeFile(CONFIG_FILE, { projects });
    }
  } else {
    projects = readConfig().projects;
  }
  // register links

  console.log(green('Setting up…'));
  await asyncForEach<Project>(projects, async ({ nodeName, path, yarn }) => {
    console.log(green(` ${nodeName}`));
    await link(path, yarn)
  });
  console.log(green('Done! ✔'));
};

export default setup;
