type CB<T = unknown> = (el: T, index: number, list: T[]) => Promise<void>;
export async function asyncForEach<T = unknown>(array: T[], callback: CB<T>): Promise<void> {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
