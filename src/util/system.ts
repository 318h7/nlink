import execa from 'execa';

type PMcommand = (command: string) => (
  path: string,
  yarn?: boolean,
  args?: string[],
) => Promise<void | string>;
export const pmCommand: PMcommand = command => async (path, yarn, args = []) => {
  try {
    const pm = yarn ? 'yarn' : 'npm';
    await execa(pm, [command, ...args], { cwd: path });
  } catch (error) {
    return (error as Error).message;
  }
}

export const link = pmCommand('link');
export const unlink = pmCommand('unlink');
export const install = pmCommand('install');
