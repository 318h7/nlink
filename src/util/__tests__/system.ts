import execa from 'execa';

import { pmCommand, link, unlink, install } from '../system';

jest.mock('execa');

describe('util/system', () => {
  const command = 'tesst';
  const path = '/test';
  const test = pmCommand(command);

  describe('pmCommand', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('calls pm command for yarn', () => {
      test(path, true);

      expect(execa).toHaveBeenCalledWith('yarn', [command], { cwd: path });
    });

    it('calls pm command for npm', () => {
      test(path, false);

      expect(execa).toHaveBeenCalledWith('npm', [command], { cwd: path });
    });

    it('calls pm command with arguments', () => {
      const args = ['foo', 'bar'];
      test(path, false, args);

      expect(execa).toHaveBeenCalledWith('npm', [command, ...args], { cwd: path });
    });

    describe('link', () => {
      it('calls link command', () => {
        link(path, true);

        expect(execa).toHaveBeenCalledWith('yarn', ['link'], { cwd: path });
      });
    });

    describe('unlink', () => {
      it('calls unlink command', () => {
        unlink(path, true);

        expect(execa).toHaveBeenCalledWith('yarn', ['unlink'], { cwd: path });
      });
    });

    describe('install', () => {
      it('calls install command', () => {
        install(path, true);

        expect(execa).toHaveBeenCalledWith('yarn', ['install'], { cwd: path });
      });

      it('passes additional arguments', () => {
        install(path, true, ['--force', '--prefer-offline']);

        expect(execa).toHaveBeenCalledWith('yarn', ['install', '--force', '--prefer-offline'], { cwd: path });
      });
    });
  });
});
