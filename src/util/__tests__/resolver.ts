import mockFS from 'mock-fs';

import { getMockArgs, mockDep, dummyExit, createMockWorld } from '../../__test__/utils';
import { CONFIG_NAME } from '../../shared';
import * as consts from '../../shared/constants';

import resolver from '../resolver';

const setMockLocation = (newLocation: string) => {
  /* eslint-disable import/namespace, @typescript-eslint/ban-ts-comment */
  // @ts-ignore
  consts.LOCATION = newLocation;
  // @ts-ignore
  consts.CONFIG_FILE = `${newLocation}/${CONFIG_NAME}`;
  /* eslint-disable */
};

const mockExit = jest.spyOn(process, 'exit').mockImplementation(dummyExit);
const mockCallback = jest.fn().mockResolvedValue({});

const resolve = resolver(mockCallback);

describe('util/resolver', () => {
  beforeEach(() => {
    setMockLocation('/test/dev');
  });
   
  afterAll(() => {
    jest.restoreAllMocks();
  });

  afterEach(() => {
    mockFS.restore();
    mockExit.mockClear();
    jest.resetAllMocks();
  })

  it('quits if no .nlink json was found', async () => {
    mockFS({ '/test/dev': {} });

    setMockLocation('/test/dev');

    await resolve(getMockArgs({ name: 'who cares?' }));

    expect(mockExit).toHaveBeenCalled();
  });

  it('does nothing if no dependencies found', async () => {
    mockFS({
      '/test/dev': createMockWorld({ name: 'test' }, { name: 'foo' }, { name: 'bar' })
    });
    setMockLocation('/test/dev');

    await resolve(getMockArgs({ name: 'foo' }));
    await mockCallback;

    expect(mockCallback).not.toHaveBeenCalled();
  });

  describe('from root folder', () => {
    it('calls link on all projects that have name in dependenices', async () => {
      mockFS({
        '/test/dev': createMockWorld(
          { name: 'test', dependencies: ['foo'] },
          { name: 'bar', dependencies: ['foo'] },
          { name: 'foo' },
        )
      });
      setMockLocation('/test/dev');

      await resolve(getMockArgs({ name: 'foo' }));
      await mockCallback;

      expect(mockCallback).toHaveBeenCalledWith([
        mockDep('/test/dev/test', false, 'foo'),
        mockDep('/test/dev/bar', false, 'foo'),
      ]);
    });

    it('calls link only on related projects', async () => {
      mockFS({
        '/test/dev': createMockWorld(
          { name: 'test', dependencies: ['foo'] },
          { name: 'bar' },
          { name: 'foo', dependencies: ['bar'] },
        )
      });
      setMockLocation('/test/dev');

      await resolve(getMockArgs({ name: 'foo' }));
      await mockCallback;

      expect(mockCallback).toHaveBeenCalledWith([mockDep('/test/dev/test', false, 'foo')]);
    });

    it('does nothing if name not found among dependencies', async () => {
      mockFS({
        '/test/dev': createMockWorld(
          { name: 'test', dependencies: ['foo'] },
          { name: 'bar', dependencies: [ 'random', 'things', 'test' ] },
          { name: 'foo', dependencies: ['bar'] },
        )
      });
      setMockLocation('/test/dev');

      await resolve(getMockArgs({ name: 'elephant' }));
      await mockCallback;

      expect(mockCallback).not.toHaveBeenCalled();
    });

    it('passing no name resolves everything in developed projects', async () => {
      mockFS({
        '/test/dev': createMockWorld(
          { name: 'test', dependencies: ['foo', 'bar'] },
          { name: 'bar', dependencies: [ 'random', 'things', 'test' ] },
          { name: 'foo', dependencies: ['bar'] },
          { name: 'shmeck', dependencies: ['nothing', 'related', 'here'] },
        )
      });
      setMockLocation('/test/dev');

      await resolve(getMockArgs());
      await mockCallback;

      expect(mockCallback).toHaveBeenCalledWith([
       mockDep('/test/dev/test', false, ['foo', 'bar']),
       mockDep('/test/dev/bar', false, ['test']),
       mockDep('/test/dev/foo', false, ['bar']),
      ]);
    });
  });

  describe('from project folder', () => {
    describe('named', () => {
      it('resolves the project if called from depender', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['foo', 'bar'] },
            { name: 'bar', dependencies: [ 'foo' ] },
            { name: 'foo', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await resolve(getMockArgs({ name: 'foo' }));
        await mockCallback;

        expect(mockCallback).toHaveBeenCalledWith([mockDep('/test/dev/bar', false, ['foo'])]);
      });

      it('resolves the project by folder name', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['le-foo', 'bar'] },
            { name: 'bar', dependencies: [ 'le-foo' ] },
            { name: 'foo', dependencies: [], nodeName: 'le-foo' },
          )
        });
        setMockLocation('/test/dev/bar');

        await resolve(getMockArgs({ name: 'foo' }));
        await mockCallback;

        expect(mockCallback).toHaveBeenCalledWith([mockDep('/test/dev/bar', false, ['le-foo'])]);
      });

      it('resolves the project when called from dependency', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['foo', 'bar'] },
            { name: 'bar', dependencies: [ 'foo' ] },
            { name: 'foo', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await resolve(getMockArgs({ name: 'test' }));
        await mockCallback;

        expect(mockCallback).toHaveBeenCalledWith([mockDep('/test/dev/test', false, 'bar')]);
      });

      it('resolves couple of projects when called from dependency', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['foo'] },
            { name: 'bar', dependencies: [ 'foo', 'test', 'spam' ] },
            { name: 'foo', dependencies: [] },
            { name: 'spam', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await resolve(getMockArgs({ name: 'foo', _: ['link', 'test', 'spam'] }));
        await mockCallback;

        expect(mockCallback).toHaveBeenCalledWith([mockDep('/test/dev/bar', false, ['foo', 'test', 'spam'])]);
      });

      it('does nothing when project has no such dependency', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['bar'] },
            { name: 'bar', dependencies: [ 'foo' ] },
            { name: 'foo', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/test');

        await resolve(getMockArgs({ name: 'foo' }));
        await mockCallback;

        expect(mockExit).toHaveBeenCalled();
      });
    });

    describe('unnamed call', () => {
      it('quits if there are no dependencies and no project depend on it', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: [] },
            { name: 'bar', dependencies: [] },
            { name: 'foo', dependencies: ['spam'] },
            { name: 'spam', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await resolve(getMockArgs());
        await mockCallback;

        expect(mockExit).toHaveBeenCalled();
      });

      it('resolves all current project dependencies', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: [] },
            { name: 'bar', dependencies: ['test'] },
          )
        });
        setMockLocation('/test/dev/bar');

        await resolve(getMockArgs());
        await mockCallback;

        expect(mockCallback).toHaveBeenCalledWith([mockDep('/test/dev/bar', false, ['test'])]);
      });

      it('resolves to itself in all projects if has no dependencies', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['bar'] },
            { name: 'bar', dependencies: [] },
            { name: 'foo', dependencies: ['spam'] },
            { name: 'spam', dependencies: ['bar'] },
          )
        });
        setMockLocation('/test/dev/bar');

        await resolve(getMockArgs());
        await mockCallback;

        expect(mockCallback).toHaveBeenCalledWith([
          mockDep('/test/dev/test', false, 'bar'),
          mockDep('/test/dev/spam', false, 'bar')
        ]);
      });

      it('prefers current project if has both depending and depends on others', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['bar'] },
            { name: 'bar', dependencies: ['test'] },
          )
        });
        setMockLocation('/test/dev/bar');

        await resolve(getMockArgs());
        await mockCallback;

        expect(mockCallback).toHaveBeenCalledWith([mockDep('/test/dev/bar', false, ['test'])]);
      });

      it('resolves multiple projects ', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            // TODO: should this really be allowed? Throw or deal safely?
            { name: 'bar', dependencies: ['test', 'foo', 'spam', 'trash'] },
            { name: 'test', dependencies: [] },
            { name: 'foo', dependencies: [] },
            { name: 'spam', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await resolve(getMockArgs());
        await mockCallback;

        expect(mockCallback).toHaveBeenCalledWith([mockDep('/test/dev/bar', false, ['test', 'foo', 'spam'])]);
      });
    });
  });
});
