import mockFS from 'mock-fs';

import { dummyExit } from '../../__test__/utils';
import {
  getFolder, isNodeProject, isTSProject, isYarnProject,
  readFile, readPackage, readConfig, writeFile, traverse,
} from '../files';

import { CONFIG_NAME, PACKAGE_FILE } from '../../shared/constants';


const mockExit = jest.spyOn(process, 'exit').mockImplementation(dummyExit);

describe('util/files', () => {
  const DUMMY_JSON = '{ "name": "foo" }';
  const DUMMY_OBJ = { name: 'foo' };

  afterAll(() => {
    jest.restoreAllMocks();
  });

  describe('getFolder', () => {
    it('returns the outermost folder name', () => {
      const folder = getFolder('/some/random/path');

      expect(folder).toBe('path');
    });
  });

  describe('isNodeProject', () => {
    it('returns true if package.json file is present', () => {
      const result = isNodeProject([ 'test.js', PACKAGE_FILE ]);

      expect(result).toBe(true);
    });

    it('returns false if package.json file is not present', () => {
      const result = isNodeProject(['foo.js', 'bar.ts']);

      expect(result).toBe(false);
    });
  });

  describe('isTSProject', () => {
    it('returns true if tsconfig.json file is present', () => {
      const result = isTSProject(['foo.js', 'tsconfig.json']);

      expect(result).toBe(true);
    });

    it('returns false if tsconfig.json file is not present', () => {
      const result = isTSProject(['foo.js', 'bar.ts']);

      expect(result).toBe(false);
    });
  });

  describe('isYarnProject', () => {
    it('returns true if yarn.lock file is present', () => {
      const result = isYarnProject(['foo.js', 'yarn.lock']);

      expect(result).toBe(true);
    });

    it('returns false if yarn.lock file is not present', () => {
      const result = isYarnProject(['foo.js', 'bar.ts']);

      expect(result).toBe(false);
    });
  });

  describe('readFile', () => {
    afterEach(() => {
      mockFS.restore();
    });

    it('returns the file if it exists', () => {
      mockFS({ '/test': { 'foo.js': DUMMY_JSON }});

      const file = readFile('/test/foo.js');

      expect(file).toStrictEqual(DUMMY_OBJ);
    });

    it('throws an error if the file does not exists', () => {
      mockFS({ '/test': { 'foo.ts': DUMMY_JSON }});

      expect(() => {
        readFile('/test/foo.js');
      }).toThrow();
    });
  });

  describe('readPackage', () => {
    afterEach(() => {
      mockFS.restore();
    });

    it('returns the package.json if it exists', () => {
      mockFS({ '/test': { [PACKAGE_FILE]: DUMMY_JSON }});

      const file = readPackage('/test');

      expect(file).toStrictEqual(DUMMY_OBJ);
    });

    it('throws an error if the file does not exists', () => {
      mockFS({ '/test': { 'foo.ts': DUMMY_JSON }});

      expect(() => {
        readFile('/test');
      }).toThrow();
    });
  });

  describe('readConfig', () => {
    afterEach(() => {
      mockFS.restore();
    });

    it('returns the nlink config file if it exists', () => {
      mockFS({ '/test': { [CONFIG_NAME]: DUMMY_JSON }});

      const file = readConfig('/test');

      expect(file).toStrictEqual(DUMMY_OBJ);
    });

    it('traverses upwards to try and find the config', () => {
      mockFS({ '/test': { [CONFIG_NAME]: DUMMY_JSON, '/test/path/to/some/dir': {} }});

      const file = readConfig('/test/path/to/some/dir');

      expect(file).toStrictEqual(DUMMY_OBJ);
    });

    it('traverses upwards up to 8 levels', () => {
      mockFS({
        '/test': { [CONFIG_NAME]: DUMMY_JSON,
        '/test/path/to/some/dir/is/too/long/now/damit': {} }}
      );

      readConfig('/test/path/to/some/dir/is/too/long/now/damit');

      expect(mockExit).toHaveBeenCalledWith(1);
    });
  });

  describe('writeFile', () => {
    afterEach(() => {
      mockFS.restore();
    });

    it('updates the json file', () => {
      mockFS({ '/test': { [CONFIG_NAME]: DUMMY_JSON }});

      writeFile('/test/.nlink.json', { name: 'bar' });
      const file = readConfig('/test');

      expect(file).toStrictEqual({ name: 'bar' });
    });
  });

  describe('traverse', () => {
    afterEach(() => {
      mockFS.restore();
    });

    it('returns empty array when callback does not return anything', () => {
      mockFS({ '/test': {} });
      const cb = jest.fn().mockReturnValue(null);
     
      const result = traverse('/test', cb);

      expect(result).toEqual([]);
    });

    it('returns callback results in a list', () => {
      mockFS({
        '/test': { 'file.txt': 'foo', 'other_file.txt': 'bar', 'one_more.txt': 'boo' },
        '/test/deeper': { 'foo.txt': 'foo', 'bar.txt': 'bar' },
      });
      const cb = jest.fn().mockImplementation((...args: Array<string | string[]>) => ({ ...args }));

      const result = traverse('/test', cb);

      expect(result).toStrictEqual([
        { 0: ['deeper', 'file.txt', 'one_more.txt', 'other_file.txt'], 1: '/test' },
        { 0: ['bar.txt', 'foo.txt'], 1: '/test/deeper' },
      ]);
    });

    it('passes files list and path to callback', () => {
      mockFS({ '/test': { 'file.txt': 'foo', 'other_file.txt': 'bar', 'one_more.txt': 'boo' } });
      const cb = jest.fn();

      traverse('/test', cb);

      expect(cb).toHaveBeenCalledWith(['file.txt', 'one_more.txt', 'other_file.txt'], '/test');
    });

    it('filters out hidden files', () => {
      mockFS({ '/test': { 'folder': {}, '.hidden': {}, '.secret': {} } });
      const cb = jest.fn();

      traverse('/test', cb);

      expect(cb).toHaveBeenNthCalledWith(1, ['.hidden', '.secret', 'folder'], '/test');
      expect(cb).toHaveBeenNthCalledWith(2, [], '/test/folder');
      expect(cb).toHaveBeenCalledTimes(2);
    });

    it('filters out dead end paths', () => {
      mockFS({
        '/test': {
          folder: {}, src: {}, bin: {}, dist: {}, build: {},
          '.git': {}, 'public': {}, resources: {}, coverage: {},
        }
      });
      const cb = jest.fn();

      traverse('/test', cb);

      expect(cb).toHaveBeenNthCalledWith(
        1,
        ['.git', 'bin', 'build', 'coverage', 'dist', 'folder', 'public', 'resources', 'src'],
        '/test'
      );
      expect(cb).toHaveBeenNthCalledWith(2, [], '/test/folder');
      expect(cb).toHaveBeenCalledTimes(2);
    });

    it('goes 8 levels deep to find projects', () => {
      const path = 'one/two/three/four/five/six/seven/eight/nine/ten';
      mockFS({ [`/${path}`]: {}});
      const cb = jest.fn();

      traverse('/one', cb);

      path.split('/').reduce((currentPath, folder, index, list) => {
        const next = index + 1;
        currentPath = currentPath + '/' + folder;
        if (index < 8) {
          expect(cb).toHaveBeenNthCalledWith(next, [list[next]], currentPath);
        }
        return currentPath;
      }, '');
      expect(cb).toHaveBeenCalledTimes(9);
    });
  });
});
