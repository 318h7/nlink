#!/usr/bin/env node
import yargs from 'yargs';

const BIN_NAME = 'nlk';

import { setup, link, unlink } from './commands';
const args = yargs
  .scriptName(BIN_NAME)
  .command('setup',
    'Creates project map in the current location',
    (yargs) => {
      yargs.option('force', {
        alias: 'f',
        describe: 'Remove the existing configuration and setup links',
      })
    },
    setup,
  )
  .command('link [name]',
    'Links project dependencies',
    (yargs) => {
      yargs
      .positional('name', {
        describe: 'Package name to link',
        defaultDescription: 'Link all packages in the project',
      })
      // .option('brute', { alias: 'b', description: 'Skip dependency lookup' })
    },
    link,
  )
  .command('unlink [name]',
    'Uninks project dependencies',
    (yargs) => {
      yargs
      .positional('name', {
        describe: 'Package name to ulink',
        defaultDescription: 'Unlink all project dependencies',
      })
    },
    unlink,
  )
//  .option(
//    'watch',
//    { alias: 'w', describe: 'Continues running nlink and unlinks dependencies on quit' },
// )
  .parse();

if (args._.length === 0) {
  console.log(
    `No command found. Run "${BIN_NAME} --help" to find the list of possible commands and options`
  );
}
