module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: ['<rootDir>/src/**/__tests__/*.ts'],
  globals: {
    'ts-jest': {
      isolatedModules: true,
    },
  },
  transform: {
    '^.+\\.(ts)?$': 'ts-jest',
  },
  setupFiles: ['<rootDir>/src/__test__/setup.ts'],
  transformIgnorePatterns: ['<rootDir>/node_modules/(?!(@schemastore))'],
  collectCoverageFrom: [
    "src/**/*.ts",
    "!**/node_modules/**",
    "!**/dist/**",
    "!**/index.ts",
    "!**/constants.ts",
    "!**/types.ts",
    "!**/__test__/**",
    "!src/nlink.ts",
  ],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 90,
      lines: 80,
      statements: 90,
    },
  },
}
