import mockFS from 'mock-fs';

import { getMockArgs, dummyExit, createMockWorld, setMockLocation, Mock } from '../../__test__/utils';
import ln from '../link';
import { link } from '../../util/system';

jest.mock('../../util/system');

const mockExit = jest.spyOn(process, 'exit').mockImplementation(dummyExit);

describe('commands/link', () => {
  beforeEach(() => {
    setMockLocation('/test/dev');
  });

  afterAll(() => {
    jest.restoreAllMocks();
  });

  afterEach(() => {
    mockFS.restore();
    const mockLink = link as Mock<typeof link>;
    mockLink.mockClear();
    mockExit.mockClear();
  })

  it('quits if no .nlink json was found', async () => {
    mockFS({ '/test/dev': {} });

    await ln(getMockArgs({ name: 'who cares?' }));
    await link;

    expect(link).not.toHaveBeenCalled();
    expect(mockExit).toHaveBeenCalled();
  });

  it('does nothing if no dependencies found', async () => {
    mockFS({
      '/test/dev': createMockWorld({ name: 'test' }, { name: 'foo' }, { name: 'bar' })
    });
    setMockLocation('/test/dev');

    await ln(getMockArgs({ name: 'foo' }));
    await link;

    expect(link).not.toHaveBeenCalled();
  });

  describe('from root folder', () => {
    it('calls link on all projects that have name in dependenices', async () => {
      mockFS({
        '/test/dev': createMockWorld(
          { name: 'test', dependencies: ['foo'] },
          { name: 'bar', dependencies: ['foo'] },
          { name: 'foo' },
        )
      });
      setMockLocation('/test/dev');

      await ln(getMockArgs({ name: 'foo' }));
      await link;

      expect(link).toHaveBeenNthCalledWith(1, '/test/dev/test', false, ['foo']);
      expect(link).toHaveBeenNthCalledWith(2, '/test/dev/bar', false, ['foo']);
    });

    it('calls link only on related projects', async () => {
      mockFS({
        '/test/dev': createMockWorld(
          { name: 'test', dependencies: ['foo'] },
          { name: 'bar' },
          { name: 'foo', dependencies: ['bar'] },
        )
      });
      setMockLocation('/test/dev');

      await ln(getMockArgs({ name: 'foo' }));
      await link;

      expect(link).toHaveBeenNthCalledWith(1, '/test/dev/test', false, ['foo']);
      expect(link).toHaveBeenCalledTimes(1);
    });

    it('does nothing if name not found among dependencies', async () => {
      mockFS({
        '/test/dev': createMockWorld(
          { name: 'test', dependencies: ['foo'] },
          { name: 'bar', dependencies: [ 'random', 'things', 'test' ] },
          { name: 'foo', dependencies: ['bar'] },
        )
      });
      setMockLocation('/test/dev');

      await ln(getMockArgs({ name: 'elephant' }));
      await link;

      expect(link).not.toHaveBeenCalled();
    });

    it('passing no name links everything in developed projects', async () => {
      mockFS({
        '/test/dev': createMockWorld(
          { name: 'test', dependencies: ['foo', 'bar'] },
          { name: 'bar', dependencies: [ 'random', 'things', 'test' ] },
          { name: 'foo', dependencies: ['bar'] },
          { name: 'shmeck', dependencies: ['nothing', 'related', 'here'] },
        )
      });
      setMockLocation('/test/dev');

      await ln(getMockArgs());
      await link;

      expect(link).toHaveBeenNthCalledWith(1, '/test/dev/test', false, ['foo', 'bar']);
      expect(link).toHaveBeenNthCalledWith(2, '/test/dev/bar', false, ['test']);
      expect(link).toHaveBeenNthCalledWith(3, '/test/dev/foo', false, ['bar']);
      expect(link).toHaveBeenCalledTimes(3);
    });
  });

  describe('from project folder', () => {
    describe('named', () => {
      it('links the project if called from depender', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['foo', 'bar'] },
            { name: 'bar', dependencies: [ 'foo' ] },
            { name: 'foo', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ln(getMockArgs({ name: 'foo' }));
        await link;

        expect(link).toHaveBeenCalledWith('/test/dev/bar', false, ['foo']);
      });

      it('links the project when called from dependency', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['foo', 'bar'] },
            { name: 'bar', dependencies: [ 'foo' ] },
            { name: 'foo', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ln(getMockArgs({ name: 'test' }));
        await link;

        expect(link).toHaveBeenCalledWith('/test/dev/test', false, ['bar']);
      });

      it('links couple of projects when called from dependency', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['foo'] },
            { name: 'bar', dependencies: [ 'foo', 'test', 'spam' ] },
            { name: 'foo', dependencies: [] },
            { name: 'spam', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ln(getMockArgs({ name: 'foo', _: ['link', 'test', 'spam'] }));
        await link;

        expect(link).toHaveBeenCalledWith('/test/dev/bar', false, ['foo', 'test', 'spam']);
      });

      it('does nothing when project has no such dependency', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['bar'] },
            { name: 'bar', dependencies: [ 'foo' ] },
            { name: 'foo', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/test');

        await ln(getMockArgs({ name: 'foo' }));
        await link;

        expect(link).not.toHaveBeenCalled();
        expect(mockExit).toHaveBeenCalled();
      });
    });

    describe('unnamed call', () => {
      it('quits if there are no dependencies and no project depend on it', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: [] },
            { name: 'bar', dependencies: [] },
            { name: 'foo', dependencies: ['spam'] },
            { name: 'spam', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ln(getMockArgs());
        await link;

        expect(mockExit).toHaveBeenCalled();
      });

      it('links all current project dependencies', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: [] },
            { name: 'bar', dependencies: ['test'] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ln(getMockArgs());
        await link;

        expect(link).toHaveBeenCalledWith('/test/dev/bar', false, ['test']);
      });

      it('links to itself in all projects if has no dependencies', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['bar'] },
            { name: 'bar', dependencies: [] },
            { name: 'foo', dependencies: ['spam'] },
            { name: 'spam', dependencies: ['bar'] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ln(getMockArgs());
        await link;

        expect(link).toHaveBeenNthCalledWith(1, '/test/dev/test', false, ['bar']);
        expect(link).toHaveBeenNthCalledWith(2, '/test/dev/spam', false, ['bar']);
        expect(link).toHaveBeenCalledTimes(2);
      });

      it('prefers current project if has both depending and depends on others', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            { name: 'test', dependencies: ['bar'] },
            { name: 'bar', dependencies: ['test'] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ln(getMockArgs());
        await link;

        expect(link).toHaveBeenCalledWith('/test/dev/bar', false, ['test']);
      });

      it('links up multiple projects ', async () => {
        mockFS({
          '/test/dev': createMockWorld(
            // TODO: should this really be allowed? Throw or deal safely?
            { name: 'bar', dependencies: ['test', 'foo', 'spam', 'trash'] },
            { name: 'test', dependencies: [] },
            { name: 'foo', dependencies: [] },
            { name: 'spam', dependencies: [] },
          )
        });
        setMockLocation('/test/dev/bar');

        await ln(getMockArgs());
        await link;

        expect(link).toHaveBeenCalledWith('/test/dev/bar', false, ['test', 'foo', 'spam']);
      });
    });
  });
});
