export interface ProjectTemplate {
  name: string,
  nodeName: string,
  path: string,
  ts: boolean,
  yarn: boolean,
  nodeDependencies?: Record<string, string>,
  depending?: string[],
  dependencies?: string[],
}

export type Project = Required<Omit<ProjectTemplate, 'nodeDependencies'>>;

export interface ProjectsFile {
  projects: Project[];
}

type Base = Pick<Project, 'path' | 'yarn'>;
export type MultiDependency = Base & { names: string[] }
export type MonoDependency = Base & { nodeName: string }

export interface Dependency {
  path: string;
  yarn: boolean;
  names?: string[];
  nodeName?: string;
}
