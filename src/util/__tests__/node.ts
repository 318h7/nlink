import { createProject, analyseDependencies } from '../node';

describe('util/node', () => {
  const name = 'test';
  const dependencies = { foo: '0.0.1' };
  const path = '/test';
  const projectName = 'test-project';

  describe('createProject', () => {
    it('creates project from given data', () => {
      const project = createProject({ name, dependencies }, true, true, path, projectName);

      expect(project).toStrictEqual({
        name: projectName, nodeName: name, nodeDependencies: dependencies, yarn: true, ts: true, path
      })
    });

    it('uses project name if name was not present', () => {
      const project = createProject({ dependencies }, true, true, path, projectName);

      expect(project).toStrictEqual({
        name: projectName, nodeName: projectName, nodeDependencies: dependencies, yarn: true, ts: true, path
      })
    });
  })

  describe('analyzeDependencies', () => {
    test('returns project template wiht filled dependencies', () => {
      const project = createProject({ name, dependencies }, true, true, path, projectName);

      const result = analyseDependencies([project]);

      expect(result)
        .toStrictEqual([{
          name: projectName, nodeName: name, dependencies: [], depending: [], yarn: true, ts: true, path
        }]);
    });

    test('creates dependenies and depending lists for projects', () => {
      const projectA = createProject({ name: 'a', dependencies: { b: '1.0.0' }}, true, true, path, 'a');
      const projectB = createProject({ name: 'b', dependencies }, true, true, path, 'b');

      const result = analyseDependencies([projectA, projectB]);

      expect(result).toStrictEqual([
        { name: 'a', nodeName: 'a', dependencies: ['b'], depending: [], yarn: true, ts: true, path },
        { name: 'b', nodeName: 'b', dependencies: [], depending: ['a'], yarn: true, ts: true, path },
      ])
    });

    test('removes unrelated dependencies', () => {
      const projectA = createProject(
        { name: 'a', dependencies: { b: '1.0.0', c: '2.0.0', d: '1.0.0' }}, true, true, path, 'a');
      const projectB = createProject({ name: 'b', dependencies }, true, true, path, 'b');

      const result = analyseDependencies([projectA, projectB]);

      expect(result).toStrictEqual([
        { name: 'a', nodeName: 'a', dependencies: ['b'], depending: [], yarn: true, ts: true, path },
        { name: 'b', nodeName: 'b', dependencies: [], depending: ['a'], yarn: true, ts: true, path },
      ])
    });

    test('does not create dependenies and depending lists for self', () => {
      const projectA = createProject({ name: 'a', dependencies: { a: '1.0.0' }}, true, true, path, 'a');
      const projectB = createProject({ name: 'b', dependencies }, true, true, path, 'b');

      const result = analyseDependencies([projectA, projectB]);

      expect(result).toStrictEqual([
        { name: 'a', nodeName: 'a', dependencies: [], depending: [], yarn: true, ts: true, path },
        { name: 'b', nodeName: 'b', dependencies: [], depending: [], yarn: true, ts: true, path },
      ])
    });

    test('creates records for interdependencies', () => {
      const projectA = createProject({ name: 'a', dependencies: { b: '1.0.0' }}, true, true, path, 'a');
      const projectB = createProject({ name: 'b', dependencies: { a: '2.0.0' }}, true, true, path, 'b');

      const result = analyseDependencies([projectA, projectB]);

      expect(result).toStrictEqual([
        { name: 'a', nodeName: 'a', dependencies: ['b'], depending: ['b'], yarn: true, ts: true, path },
        { name: 'b', nodeName: 'b', dependencies: ['a'], depending: ['a'], yarn: true, ts: true, path },
      ])
    });

    test('creates records for multiple dependencies', () => {
      const projectA = createProject(
        { name: 'a', dependencies: { b: '1.0.0', c: '2.0.0' }}, true, true, path, 'a'
      );
      const projectB = createProject({ name: 'b', dependencies: { c: '2.0.0' }}, true, true, path, 'b');
      const projectC = createProject({ name: 'c', dependencies: { foo: '2.0.0' }}, true, true, path, 'c');

      const result = analyseDependencies([projectA, projectB, projectC]);

      expect(result).toStrictEqual([
        { name: 'a', nodeName: 'a', dependencies: ['b', 'c'], depending: [], yarn: true, ts: true, path },
        { name: 'b', nodeName: 'b', dependencies: ['c'], depending: ['a'], yarn: true, ts: true, path },
        { name: 'c', nodeName: 'c', dependencies: [], depending: ['a', 'b'], yarn: true, ts: true, path },
      ])
    });
  });
});
