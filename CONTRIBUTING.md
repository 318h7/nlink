# Contributing

Welcome stranger, I'm very glad that you're here.
The project is open for contributions, [suggestions](https://gitlab.com/318h7/nlink/-/requirements_management/requirements), [issues](https://gitlab.com/318h7/nlink/-/issues) or anything you would like to help. ❤ 

If you would like to contribute, please open a [merge request](https://docs.gitlab.com/ee/user/project/repository/forking_workflow).

## Development ⚙

### One shot test

To run the command once, use `yarn nlk <parameters>`
But it's mostly useful as `--help` command test.

### Watch mode

To run the compiler on the watch mode run `yarn dev`
This will recompile on change to the *dist* folder and make the file executable.

You can launch the *nlink.js* from the *dist* folder or better run `npm link` to make the `nlk` executable
accessible system-wide for testing.

Then you can call the tool from any place.

## Tests

### Info

The project is using [jest](https://jestjs.io/) for tests and [mock-fs](https://github.com/tschaub/mock-fs)
with custom helpers (found in [test utils](src/__test__/utils.ts)) for creating mock file system structures with predefined `package.json` and `.link.json` config files.

### Coverage

The coverage thresholds are set to 80-90% and I would like to keep them high. If you do contribute, please make sure to cover your functionality with tests. 

## Release

The project is using [semantic release](https://github.com/semantic-release/semantic-release)
No need to bump versions, or update the change log. Just follow the rules of the [conventional-commits](https://www.conventionalcommits.org/en/v1.0.0/).

Whole release is automated for the `master` branch.
This means that anything that goes into `master` is supposed to be stable.
