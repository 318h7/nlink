import { Arguments } from 'yargs';
import { green } from 'chalk';

import { Dependency } from '../shared';
import { unlink as nodeUnlink, install, resolverFactory, asyncForEach } from '../util'

interface UnlinkParams {
  name?: string,
}

type UnlinkProjects = (dependenices: Dependency[]) => Promise<void>;
const unlinkProjects: UnlinkProjects = async (dependencies) => {
  await asyncForEach<Dependency>(
    dependencies,
    async (dependency) => {
      if (dependency) {
        const { path, yarn, nodeName, names } = dependency;
        console.log(green(`Unlinking ${nodeName ?? names}…`));
        await nodeUnlink(path, yarn, nodeName ? [nodeName] : names );
        console.log(green('Reinstalling dependencies…'));
        await install(path, yarn, ['--force', '--prefer-offline']);
      }
    }
  );
  console.log(green('Done! ✔'));
};

type Unlink = (params: Arguments<UnlinkParams>) => Promise<void>;
const unlink: Unlink = async ({ name, _, ...rest }) => {
  const restArgs = _.filter(name => name !== 'unlink');
  const unlinkResolver = resolverFactory(unlinkProjects);

  await unlinkResolver({ name, _: restArgs, ...rest });
};

export default unlink;
