import { flow } from 'lodash';

import { Project, PackageJSON, ProjectTemplate } from '../shared';

type ExtractDependencyNames = (project: ProjectTemplate) => ProjectTemplate;
const extractDependencyNames: ExtractDependencyNames = ({
  nodeDependencies,
  ...rest
}) => ({
  dependencies: Object.keys(nodeDependencies || {}),
  ...rest
})

type CreateDependenices = (projects: ProjectTemplate[]) => ProjectTemplate[];
const createDependencies: CreateDependenices = projects => projects.map(extractDependencyNames);

type ExtractName = (projects: ProjectTemplate) => string;
const extractName: ExtractName = ({ nodeName }) => nodeName;

type FilterUnrelated = (developed: string[]) => (
  project: ProjectTemplate,
) => ProjectTemplate;
const filterUnderlated: FilterUnrelated = developed => (
  { name: projectName, dependencies, ...rest }
) => {
  const filteredDependencies = dependencies?.filter(
    (dependencie) => developed.some((name) => name === dependencie && name !== projectName)
  );
  return { name: projectName, dependencies: filteredDependencies, ...rest };
}

type RemoveUnrelatedDependencies = (projects: ProjectTemplate[]) => ProjectTemplate[];
const removeUnrelatedDependenices: RemoveUnrelatedDependencies = projects => projects.map(
  filterUnderlated(projects.map(extractName))
);

type CollectDependingOnProject = (name: string) => (
  depending: string[],
  project: ProjectTemplate,
) => string[];
const collectDependingOnProject: CollectDependingOnProject = name => (
  depending,
  { dependencies, nodeName },
) => {
  if (nodeName !== name && dependencies?.some((dependencie) => dependencie === name)) {
    depending.push(nodeName);
  }
  return depending;
}

type GetDepending = (name: string, projects: ProjectTemplate[]) => string[]
const getDepending: GetDepending = (name, projects) =>
  projects.reduce<string[]>(collectDependingOnProject(name), []);

type ExtractDepending = (
  project: ProjectTemplate,
  index: number,
  list: ProjectTemplate[]
) => Project;
const extractDepending: ExtractDepending = (
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  { nodeName, nodeDependencies, dependencies, ...rest },
  _,
  projects,
) => ({
  nodeName,
  dependencies: (dependencies as string[]),
  depending: getDepending(nodeName, projects),
  ...rest
});

type CreateDepending = (projects: ProjectTemplate[]) => Project[]
const createDepending: CreateDepending = projects => projects.map<Project>(extractDepending);

type AnalyzeDependencies = (projects: ProjectTemplate[]) => Project[];
export const analyseDependencies: AnalyzeDependencies = flow(
  createDependencies,
  removeUnrelatedDependenices,
  createDepending,
);

type CreateProject = (
  config: PackageJSON,
  yarn: boolean,
  ts: boolean,
  path: string,
  name: string,
) => ProjectTemplate;
export const createProject: CreateProject = (
  { name: nodeName, dependencies: nodeDependencies },
  yarn,
  ts,
  path,
  name,
) => ({ name, nodeName: nodeName || name, nodeDependencies, path, yarn, ts });
