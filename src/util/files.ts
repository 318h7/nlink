import fs from 'fs';
import { flow } from 'lodash';
import { red } from 'chalk';

import {
    PackageJSON, ProjectsFile,
    PACKAGE_FILE, YARN_LOCK, TS_CONFIG, DEAD_ENDS, LOCATION, CONFIG_NAME,
} from '../shared';

const TRAVERSE_DEPTH = 8;

type FileConverter = (files: string[]) => string[];
type FileChecker = (files: string[]) => boolean;

type IsFolder = (path: string) => boolean;
const isFolder: IsFolder = path => fs.existsSync(path) && fs.lstatSync(path).isDirectory();

type IsFile = (path: string) => boolean;
export const isFile: IsFile = path => fs.existsSync(path) && fs.lstatSync(path).isFile();

const filterDeadEnds: FileConverter = (files: string[]) => files.filter(
  name => !DEAD_ENDS.some(folder => folder === name)
);

const filterHidden: FileConverter = files => files.filter(name => name.charAt(0) !== '.');
const filterFolders: FileConverter = files => files.filter(isFolder);

type ConvertToAbsolute = (path: string) => (files: string[]) => string[];
const convertToAbsolute: ConvertToAbsolute = path => files => files.map(
  file => `${path}/${file}`
);

type GetFolders = (path: string) => (files: string[]) => string[];
const getFolders: GetFolders = path => files => flow(
  convertToAbsolute(path),
  filterFolders,
)(files);

export const isNodeProject: FileChecker = files => files.some(name => name === PACKAGE_FILE);
export const isTSProject: FileChecker = files => files.some(name => name === TS_CONFIG);
export const isYarnProject: FileChecker = files => files.some(
  name => name === YARN_LOCK
);

type GetFolder = (path: string) => string;
export const getFolder: GetFolder = path => path.split('/').pop() || '';

type MoveUp = (path: string) => string;
const moveUp: MoveUp = path => {
  const folders = path.split('/');
  folders.pop();

  return folders.join('/');
};

type ReadConfig = (
  location?: string,
  depth?: number,
) => ProjectsFile;
export const readConfig: ReadConfig = (location = LOCATION, depth = TRAVERSE_DEPTH) => {
  try {
    return readFile<ProjectsFile>(`${location}/${CONFIG_NAME}`);
  } catch (_) {
    if (depth > 0 && location !== '/') {
      return readConfig(moveUp(location), depth - 1);
    } else {
      console.log(red('Error: Unable to find config file.'));
      process.exit(1);

      // for testing. For the horde!
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      return { projects: null } as any;
    }
  }
}

type ReadPackage = (path: string) => PackageJSON;
export const readPackage: ReadPackage = path => readFile<PackageJSON>(`${path}/${PACKAGE_FILE}`);

type ReadFile = <T = Record<string, unknown>>(path: string) => T;
export const readFile: ReadFile = path => {
  const rawFile = fs.readFileSync(path);
  return JSON.parse(rawFile.toString());
}

type WriteFile = (path: string, data: Record<string, unknown>) => void;
export const writeFile: WriteFile = (path, data) => {
  fs.writeFileSync(path, JSON.stringify(data));
}

type Callback<T = Record<string, unknown>> = (files: string[], path: string) => T | null;

export const traverse = <T = Record<string, unknown>>(
  path: string,
  callback: Callback<T>,
  depth = TRAVERSE_DEPTH
): T[] => {
  let collection: T[] = [];
  const files = fs.readdirSync(path);

  const object: T | null = callback(files, path);
  if (object) {
    collection.push(object);
  }

  if (depth > 0) {
    const folders = flow(
      filterDeadEnds,
      filterHidden,
      getFolders(path),
    )(files);

    folders.forEach(folder => {
      collection = [
        ...collection,
        ...traverse<T>(folder, callback, depth - 1)
      ];
    });
  }

  return collection;
}
