import { Arguments } from 'yargs';
import { green } from 'chalk';

import { Dependency } from '../shared';
import { link as nodeLink, resolverFactory, asyncForEach } from '../util'

interface LinkParams {
  name?: string,
}

type LinkProjects = (dependenices: Dependency[]) => Promise<void>;
const linkProjects: LinkProjects = async (dependencies) => {
  asyncForEach(
    dependencies,
    async (dependency) => {
      if (dependency) {
        const { path, yarn, nodeName, names } = dependency;
        console.log(green(`Linking ${nodeName ?? names}…`));
        await nodeLink(path, yarn, nodeName ? [nodeName] : names );
      }
    }
  );
  console.log(green('Done! ✔'));
};

type Link = (params: Arguments<LinkParams>) => Promise<void>;
const link: Link = async ({ name, _, ...rest }) => {
  const restArgs = _.filter(name => name !== 'link');
  const linkResolver = resolverFactory(linkProjects);

  await linkResolver({ name, _: restArgs, ...rest });
};

export default link;
