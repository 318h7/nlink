import { Arguments } from 'yargs';
import { red, bold } from 'chalk';
import { isEmpty, isNil, flow } from 'lodash';

import { readConfig, getFolder, isFile } from '../util'
import { Project, CONFIG_FILE, LOCATION, Dependency } from '../shared';

interface MockParams {
  name?: string,
}

type IsNothing = (something: unknown) => boolean;
const isNothing: IsNothing = something => isNil(something) || isEmpty(something);
const isProject = (name: string) => (
  { name: folder, nodeName, path }: Project
) => folder === name || nodeName.includes(name) || getFolder(path).includes(name);

type GetProject = (projects: Project[], name: string) => Project | never;
const getProject: GetProject = (projects, name) => {
  const project = projects.find(isProject(name));
  if (isNothing(project)) {
    console.log(red(`Error: Unable to find project ${bold(name)}`));
    process.exit(1);

    // for testing
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return { nodeName: null, yarn: null, path: null } as any;
  }

  return project as Project;
};

// const containsTS = (projects: Project[]) => projects.some(({ ts }) => ts);

// const isProjectFolder = ({ path }: Project) => LOCATION === path;

type IsMultirepoFolder = () => boolean;
const isMultirepoFolder: IsMultirepoFolder = () => isFile(CONFIG_FILE);

// type GetNodeName = (projects: Project[], projectFolder: string) => string;
// const getNodeName: GetNodeName = (
//   projects,
//   projectFolder
// ) => projects.find(({ name }) => name === projectFolder)?.nodeName ?? ''; // will never get empty

type GetDependencies = (project: Project[], nodeName: string) => Dependency[];
const getDependencies: GetDependencies = (projects, nodeName) => {
  const filterUrelated = (projects: Project[]) => projects.filter(
    ({ dependencies }) => dependencies?.some(dependency => dependency === nodeName)
  );
  const projectToDependency = (projects: Project[]) => projects.map<Dependency>(
    ({ path, yarn }) => ({ yarn, path, nodeName })
  );

  return flow(
    filterUrelated,
    projectToDependency
  )(projects);
}

type ContextDependencies = (project: Required<Project>, projects: Project[]) => Dependency[] | undefined;
const contextDependencies: ContextDependencies = (
  { dependencies, depending, ...context },
  projects,
) => {
  const dependingToLinks = (name: string): Dependency => {
    const { path, yarn } = getProject(projects, name);
    return { nodeName: context.nodeName, path, yarn };
  };
  const dependenciesToLink = (dependency: Dependency, name: string): Dependency => {
    const { nodeName } = getProject(projects, name);
    if (nodeName) {
      dependency?.names?.push(nodeName);
    }

    return dependency;
  };

  if (isEmpty(depending) && isEmpty(dependencies)) {
    console.log('No dependencies or depending projects. Nothing to do here, quitting…');
    process.exit(0);
  }

  if (!isEmpty(dependencies)) {
    const dependenice = dependencies?.reduce<Dependency>(
      dependenciesToLink,
      { path: context.path, yarn: context.yarn, names: [] }
    );
    return [dependenice];
  }
  if (!isEmpty(depending)) {
    return depending?.map<Dependency>(dependingToLinks);
  }
};

type ContextDependency = (args: Array<string>, projects: Project[]) => Dependency;
const contextDependency: ContextDependency = (args, projects) => {
  const name = args[0];
  const currentProject = getProject(projects, getFolder(LOCATION));
  
  const related = args.reduce<string[]>(
    (result, arg) => {
       const argument = getProject(projects, arg);
       if (!isNil(argument) && currentProject.dependencies.some(dep => dep === argument.nodeName)) {
         result.push(argument.nodeName);
       }
       return result;
    },
    []
  );

  if (related.length) {
    const { yarn, path } = currentProject;
    return { path, yarn, names: related };
  } else  {
    const namedProject = getProject(projects, name);
    const { nodeName } = currentProject;
  if (namedProject.dependencies?.some(dep => dep === nodeName)) {
    const { yarn, path } = namedProject;
    return { nodeName, yarn, path  };
  } else {
    console.log(`Projects ${nodeName} and ${namedProject.nodeName} are not interdependent, quitting…`);
    process.exit(0);
    }
  }
};

type GetAll = (projects: Project[]) => Dependency[];
const getAll: GetAll = projects => projects.reduce<Dependency[]>(
  (result, { dependencies, path, yarn }, _, array) => {
    const related = dependencies.filter(
      (tested) => array?.some(({ nodeName: developed }) => developed === tested)
    );
    if (related.length) {
      result.push({ path, yarn, names: related });
    }
    return result;
  },
  [],
);

type ResolveDependenices = (args: Array<string | undefined>, projects: Project[]) => Dependency[];
const resolveDependencies: ResolveDependenices = ([name, ...rest], projects) => {
  const isRoot = isMultirepoFolder();

  if (isRoot && name) {
    const { nodeName } = getProject(projects, name);
    return getDependencies(projects, nodeName);
  } else if (isRoot) {
    return getAll(projects);
  } else if (name) {
    const args: unknown = [name, ...rest].filter(param => !isNil(param));
     return [contextDependency(args as string[], projects)];
  } else {
    const project = getProject(projects, getFolder(LOCATION));
    return contextDependencies(project, projects) || [];
  }
}

type ResolverCallback = (dependencies: Dependency[]) => Promise<void>;
type Resolver = (callback: ResolverCallback) => (params: Arguments<MockParams>) => Promise<void>;
const resolver: Resolver = callback => async ({ name, _: args }) => {
  const { projects } = readConfig();
  if (projects) {
    const dependencies = resolveDependencies([name, ...args], projects);

    if (dependencies && dependencies?.length) {
      await callback(dependencies);
    }
  }
};

export default resolver;
